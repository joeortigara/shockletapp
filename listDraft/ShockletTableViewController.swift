//
//  ShockletTableViewController.swift
//  listDraft
//
//  Created by Joseph Ortigara on 8/18/19./Users/joe/Documents/swiftFiles/listDraft/listDraft/ShockletTableViewController.swift
//  Copyright © 2019 Joseph Ortigara. All rights reserved.
//

import UIKit

public struct Shocklet {
    var title: String
    var image: String
    var result: String
}

struct globalShocklet {
    static var list: [Shocklet] = []
}


class ShockletTableViewCell: UITableViewCell {
    @IBOutlet weak var shockletTitleLabel: UILabel!
    @IBOutlet weak var shockletImageView: UIImageView!
    
}

class ShockletTableViewController: UITableViewController {
    
    var shockletSelected: Shocklet?
    
    public var shocklets: [Shocklet] = [
        Shocklet(title: "Type I Positive", image: "type1positive", result: "typeiPositive"),
        Shocklet(title: "Type I Negative", image: "type1negative", result: "typeiNegative"),
        Shocklet(title: "Type II Positive", image: "type2positive", result: "typeiiPositive"),
        Shocklet(title: "Type II Negative", image: "type2negative", result: "typeiiNegative"),
    ]


    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return shocklets.count
    }
    
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        performSegue(withIdentifier: "unwindToMain", sender: self)
//
//    }
//
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let indexPath = tableView.indexPathForSelectedRow {
//            datasetSelected = datasets[indexPath.row]
//        }
//    }
        
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "unwindToMain", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathForSelectedRow {
            shockletSelected = shocklets[indexPath.row]
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath) as! ShockletTableViewCell
        
        let shocklet = shocklets[indexPath.row]
        
        cell.shockletTitleLabel?.text = shocklet.title
        cell.shockletImageView?.image = UIImage(named: shocklet.image)
        return cell
    }


}
