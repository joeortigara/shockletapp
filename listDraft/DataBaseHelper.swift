//
//  DataBaseHelper.swift
//  listDraft
//
//  Created by Joseph Ortigara on 2/10/20.
//  Copyright © 2020 Joseph Ortigara. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DataBaseHelper {
    static var shareInstance = DataBaseHelper()
    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    func saveImage(data: Data) {
    var imageInstance = Image(context: context)
    imageInstance.img = data
    do {
    try context.save()
    print("Image is saved")
    } catch {
    print(error.localizedDescription)
          }
       }
    
    func fetchImage() -> [Image] {
        var fetchingImage = [Image]()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Image")
        do {
        fetchingImage = try context.fetch(fetchRequest) as! [Image]
        } catch {
        print("Error while fetching the image")
        }
        return fetchingImage
    }
}
