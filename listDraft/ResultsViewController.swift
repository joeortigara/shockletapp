//
//  ResultsViewController.swift
//  listDraft
//
//  Created by Joseph Ortigara on 8/25/19.
//  Copyright © 2019 Joseph Ortigara. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController {
    
    var dataset: Dataset?
    var shocklet: Shocklet?
    var idArray: [String]?
    
    @IBOutlet weak var datasetLabel: UILabel!
    @IBOutlet weak var shockletLabel: UILabel!
    @IBOutlet weak var resultView: UIImageView!
    
    


    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewdidload")
        
        datasetLabel.text = "Dataset: \(dataset!.title)"
        shockletLabel.text = "Shocklet: \(shocklet!.title)"
        resultView.image = UIImage(named: shocklet!.result)
        
        
        if let idArrayTest = idArray {
            print(idArrayTest)
            //for i in idArrayTest {
            //    arrayText.text += "id for user: " + idArrayTest[i] + "\n"
        //}

        // Do any additional setup after loading the view.
    }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
