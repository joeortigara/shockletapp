//
//  ViewController.swift
//  listDraft
//
//  Created by Joseph Ortigara on 8/18/19.
//  Copyright © 2019 Joseph Ortigara. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var addNewShockletButton: UIButton!
    
    
    @IBOutlet weak var analyzeButton: UIButton!
    var dataset: Dataset?
    var shocklet: Shocklet?
    var idArray: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        dataset = nil
        shocklet = nil
        analyzeButton.alpha = 0.6
        analyzeButton.isEnabled = false
    }
    
    @IBAction func addShape(_ sender: Any) {
        performSegue(withIdentifier: "toDraw", sender: sender)
    }
    
    @IBAction func showAlert() {
        
    }
    
    @IBAction func unwindToMain(sender: UIStoryboardSegue) -> Void {
        if let controller = sender.source as? DatasetTableViewController, let importedDataset = controller.datasetSelected{
            dataset = importedDataset
            print(dataset)
            
        }
        if let controller = sender.source as? ShockletTableViewController, let importedShocklet = controller.shockletSelected {
            shocklet = importedShocklet
            print(shocklet)
        }
        if dataset != nil && shocklet != nil {
            analyzeButton.alpha = 1.0
            analyzeButton.isEnabled = true
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ResultsViewController {
            let vc = segue.destination as? ResultsViewController
            
            vc?.dataset = dataset
            vc?.shocklet = shocklet
            vc?.idArray = idArray
        }
    }
    
    func httpPost() -> Void {
        let parameters = ["dataset" : dataset!.title , "shocklet" : shocklet!.title]
        
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/posts") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return}
        request.httpBody = httpBody
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                } catch {
                    print(error)
                }
            }
            
            }.resume()
    }
    
    func httpGet() -> Void {
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/todos") else { return }
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            if let response = response {
                print("GET REQUESTS RESPONSE: ")
                print(response)
            }
            
            if let data = data {
                print("GET REQUESTS DATA: ")
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    print(json)
                    
                    guard let parsedArray = json as? [Any] else { return }
                    
                    for user in parsedArray {
                        guard let userDict = user as? [String: Any] else { return }
                        guard let userId = userDict["id"] as? Int else { print("not a string"); return }
                        guard let name = userDict["title"] as? String else { return }
                        guard let completed = userDict["completed"] as? Int else { print("not an int"); return }
                    
                        print(userId)
                        print(name)
                        print(completed)
                        
                        self.idArray.append(name)
                    
                    }
                }
                catch {
                    print(error)
                }
            }
        }.resume()
        
    }
    
    
    @IBAction func optionsToResults(_ sender: Any) {
        //httpPost()
        //httpGet()
        
        
        
        
        
        print("action tripped")
        performSegue(withIdentifier: "optionsToResults", sender: self)
        
    }
    
    
}

