//
//  DatasetTableViewController.swift
//  listDraft
//
//  Created by Joseph Ortigara on 8/18/19.
//  Copyright © 2019 Joseph Ortigara. All rights reserved.
//

import UIKit

/*
struct Dataset {
    var title: String
    var image: String
    var result: String
    
}
*/


class DatasetTableViewCell: UITableViewCell {
    @IBOutlet weak var datasetTitleLabel: UILabel!
    @IBOutlet weak var datasetImageView: UIImageView!
    
    
}

class DatasetTableViewController: UITableViewController {
    
    var datasetSelected: Dataset?
    

//    func sendDatatoViewController(myString: String) {
//        let Vc = parent as! ViewController
//        Vc.dataFromContainer(containerData: myString)
//    }
    /*
    var datasets = [
        Dataset(title: "S&P 500", image: "sp500", result: "sp500Result"),
        Dataset(title: "Apple", image: "aapl", result: "aaplResult"),
        Dataset(title: "Amazon", image: "amzn", result: "amznResult")
    ]
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        //sendDatatoViewController(myString: "Test container")

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return datasets.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "unwindToMain", sender: self)
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathForSelectedRow {
            datasetSelected = datasets[indexPath.row]
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath) as! DatasetTableViewCell

        let dataset = datasets[indexPath.row]

        cell.datasetTitleLabel?.text = dataset.title
        cell.datasetImageView?.image = UIImage(named: dataset.image)
        return cell
    }
    
    

}
