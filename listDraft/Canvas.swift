//
//  Canvas.swift
//  listDraft
//
//  Created by Joseph Ortigara on 1/22/20.
//  Copyright © 2020 Joseph Ortigara. All rights reserved.
//

import UIKit

class Canvas: UIView {
    override func draw(_ rect: CGRect) {
        // custom drawing
        super.draw(rect)
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        // here are my lines
        // dummy data
        //let startPoint = CGPoint(x:0, y: 0)
        //let endPoint = CGPoint(x: 100, y: 100)
        
        //context.move(to: startPoint)
        //context.addLine(to: endPoint)
        
        context.setStrokeColor(UIColor.black.cgColor)
        context.setLineWidth(10)
        context.setLineCap(.butt)
        
        context.strokePath()
        
        lines.forEach { (line) in
            for (i,p) in line.enumerated() {
                if i==0 {
                    context.move(to: p)
                } else {
                    context.addLine(to: p)
                }
            }
        }
        

        context.strokePath()
        
    }
    
    //var line = [CGPoint]()
    var lines = [[CGPoint]]()
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        lines.append([CGPoint]())
    }
    
    
    // track the finger as we move across screen
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard var point = touches.first?.location(in: nil) else {
            return
        }
        point.x = point.x - 30
        point.y = point.y - 130
        guard var lastLine = lines.popLast() else { return }
        lastLine.append(point)
        lines.append(lastLine)
        
        //var lastLine = lines.last
        //lastLine?.append(point)
        
        //line.append(point)
        
        setNeedsDisplay()
        
    }
}
