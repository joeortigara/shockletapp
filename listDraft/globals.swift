//
//  globals.swift
//  listDraft
//
//  Created by Joseph Ortigara on 2/9/20.
//  Copyright © 2020 Joseph Ortigara. All rights reserved.
//

import Foundation
import UIKit

struct Dataset {
    var title: String
    var image: String
    var result: String
    
}

var datasets = [
    Dataset(title: "S&P 500", image: "sp500", result: "sp500Result"),
    Dataset(title: "Apple", image: "aapl", result: "aaplResult"),
    Dataset(title: "Amazon", image: "amzn", result: "amznResult")
]
